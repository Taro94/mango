using ArchPackagesViewer.Enums;
using ArchPackagesViewer.Interfaces;
using Commons;

namespace ArchPackagesViewer;

/// <summary>
/// Type for comparing versions of packages expressed as strings.
/// </summary>
public class VersionComparer : IVersionComparer
{
    /// <summary>
    /// Checks if a given version string represents a version higher than another.
    /// </summary>
    /// <param name="versionToCheck">Version to check.</param>
    /// <param name="versionToCompareWith">Version to check against.</param>
    /// <returns>ComparisonResult enum.</returns>
    /// <exception cref="ArgumentException">When either parameter contains a space character.</exception>
    public ComparisonResult CompareVersions(string versionToCheck, string versionToCompareWith)
    {
        if (versionToCheck.Contains(' ') || versionToCompareWith.Contains(' '))
        {
            throw new ArgumentException($"One of the compared version strings ('{versionToCheck}', '{versionToCompareWith}') contains a space character.");
        }

        var result = $"vercmp {versionToCheck} {versionToCompareWith}".Bash();
        return (ComparisonResult)int.Parse(result);
    }

    /// <summary>
    /// Checks if a given version fulfills the given version requirement.
    /// </summary>
    /// <param name="versionToCheck">Version string to check.</param>
    /// <param name="versionRequirement">Version requirement to check against, should start with comparison and be followed by a version string.</param>
    /// <returns>Boolean value denoting if the version meets the specified requirement. Always true if versionToCheck is an empty string.</returns>
    /// <exception cref="ArgumentException">Thrown when the version requirement does not start with the comparison character(s).</exception>
    public bool IsVersionValid(string versionToCheck, string versionRequirement)
    {
        if (versionRequirement == string.Empty)
            return true;
        if (versionToCheck == string.Empty)
            return false;

        var (expectedComparison, versionToCompareWith) = SplitIntoComparisonAndValue(versionRequirement);

        string comparisonResult;
        var comparison = CompareVersions(versionToCheck, versionToCompareWith);
        if (comparison == ComparisonResult.Equal)
            comparisonResult = "=";
        else if (comparison == ComparisonResult.LargerThan)
            comparisonResult = ">";
        else
            comparisonResult = "<";

        return expectedComparison.Contains(comparisonResult);
    }

    /// <summary>
    /// Checks if a given version range fulfills the given version requirement.
    /// </summary>
    /// <param name="versionRangeToCheck">Version range string to check, should start with a comparison and be followed by a version string.</param>
    /// <param name="versionRequirement">Version requirement to check against, should start with comparison and be followed by a version string.</param>
    /// <returns>Boolean value denoting if the version meets the specified requirement. Always true if versionToCheck is an empty string.</returns>
    /// <exception cref="ArgumentException">Thrown when the version requirement does not start with the comparison character(s).</exception>
    public bool IsVersionRangeValid(string versionRangeToCheck, string versionRequirement)
    {
        var (comparisonToCheck, valueToCheck) = SplitIntoComparisonAndValue(versionRangeToCheck);
        var (expectedComparison, valueToCompareWith) = SplitIntoComparisonAndValue(versionRequirement);

        var comparison = CompareVersions(valueToCheck, valueToCompareWith);
        if (comparison == ComparisonResult.Equal)
        {
            return comparisonToCheck.Contains('=') && expectedComparison.Contains('=');
        }

        if (comparison == ComparisonResult.LessThan)
        {
            return !comparisonToCheck.Contains('<') || !expectedComparison.Contains('>');
        }
        
        // if (comparison == ComparisonResult.LargerThan)
        return !comparisonToCheck.Contains('>') || !expectedComparison.Contains('<');
    }

    private (string, string) SplitIntoComparisonAndValue(string versionRange)
    {
        var possibleComparisons = new List<string>
        {
            ">",
            "<",
            "=",
            ">=",
            "<="
        };
        var comparison = possibleComparisons
            .Where(versionRange.StartsWith)
            .MaxBy(x => x.Length);
        
        if (comparison == null)
        {
            throw new ArgumentException(
                $"Version range {versionRange} does not start with a comparison sign (>, <, =, >=, <=)");
        }
        
        var value = versionRange.Replace(comparison, string.Empty);

        return (comparison, value);
    }
}