using ArchPackagesViewer.Interfaces;

namespace ArchPackagesViewer;

/// <inheritdoc />
public class ConflictDetector : IConflictDetector
{
    private readonly ISystemPackages _systemPackages;
    private readonly IVersionComparer _versionComparer;

    public ConflictDetector(ISystemPackages systemPackages, IVersionComparer versionComparer)
    {
        _systemPackages = systemPackages;
        _versionComparer = versionComparer;
    }

    /// <inheritdoc />
    public HashSet<IPackage> GetInstalledConflictingPackages(
        IEnumerable<string> packagesAndProvisionsToCheckAgainstInstalled,
        IEnumerable<string> conflictsToCheckAgainstInstalled)
    {
        var conflictingInstalledPackages = new HashSet<IPackage>();

        // If there is a package X installed that has a defined conflict with one of the input member packages, mark X as conflicting
        foreach (var package in packagesAndProvisionsToCheckAgainstInstalled)
        {
            var split = package.Split(' ');
            var name = split[0];
            var version = split.Length > 1 ? split[1] : string.Empty;

            foreach (var installedConflict in _systemPackages.GetInstalledPackagesConflictingWithPackage(name, version))
                conflictingInstalledPackages.Add(installedConflict);
        }

        // If any input member package has a defined conflict with an installed package X, mark X as conflicting
        foreach (var conflict in conflictsToCheckAgainstInstalled)
        {
            var possibleComparisons = new List<string>
            {
                ">",
                "<",
                "=",
                ">=",
                "<="
            };
            var comparison = possibleComparisons
                .Where(conflict.Contains)
                .MaxBy(x => x.Length);

            string name;
            string version;
            if (comparison == null)
            {
                name = conflict;
                version = string.Empty;
            }
            else
            {
                var split = conflict.Split(comparison);
                name = split[0];
                version = split[1];
            }

            var installed = _systemPackages[name];
            if (installed != null)
            {
                conflictingInstalledPackages.Add(installed);
                continue;
            }

            var providingInstalled = _systemPackages
                .GetInstalledPackagesProvidingPackage(name)
                .Where(x => version == string.Empty || _versionComparer.IsVersionRangeValid($"{comparison}{version}", x.ProvidedVersion))
                .Select(x => _systemPackages[x.ProviderName]);
            foreach (var providerOfConflict in providingInstalled)
                conflictingInstalledPackages.Add(providerOfConflict);
        }

        return conflictingInstalledPackages;
    }
}