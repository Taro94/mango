using ArchPackagesViewer.Interfaces;

namespace ArchPackagesViewer;

/// <inheritdoc />
public class PackageFactory : IPackageFactory
{
    /// <inheritdoc />
    public IPackage CreatePackage()
    {
        return new Package();
    }
}