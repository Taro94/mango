using System.Collections;
using System.Collections.Immutable;
using ArchPackagesViewer.Interfaces;

namespace ArchPackagesViewer;

/// <summary>
/// Type representing a collection of packages installed in the system.
/// </summary>
public class SystemPackages : ISystemPackages
{
    private readonly IVersionComparer _versionComparer;
    private readonly Dictionary<string, IPackage> _packagesDict = new();
    private readonly Dictionary<string, HashSet<PackageProvider>> _providersPerProvidedPackageDict = new();
    private readonly Dictionary<string, HashSet<IPackage>> _installedPackagesPerConflictingPackageName = new();

    /// <summary>
    /// Create the system packages structure representing the currently installed packages in the ArchLinux-based operating system.
    /// </summary>
    /// <param name="packageFactory">Factory for IPackage objects.</param>
    /// <param name="versionComparer">Instance of a version comparer to be used.</param>
    /// <param name="nativePackageNamesListProvider">Provider for the list of names of packages considered native by the OS.</param>
    public SystemPackages(IPackageFactory packageFactory, IVersionComparer versionComparer, INativePackagesListProvider nativePackageNamesListProvider)
    {
        _versionComparer = versionComparer;

        var dirPath = "/var/lib/pacman/local/";
        foreach (var subdirPath in Directory.GetDirectories(dirPath))
        {
            var descFilePath = Path.Combine(subdirPath, "desc");
            if (!File.Exists(descFilePath))
                continue;

            var package = packageFactory.CreatePackage();
            var sectionsToProcess = new HashSet<string>
            {
                "%NAME%",
                "%VERSION%",
                "%OPTDEPENDS%",
                "%DEPENDS%",
                "%PROVIDES%",
                "%CONFLICTS%"
            };
            var descFileLines = File.ReadAllLines(descFilePath).ToList();
            for (var i = 0; i < descFileLines.Count; i++)
            {
                if (sectionsToProcess.Contains(descFileLines[i]))
                    ProcessFileSection(package, descFileLines, i, out i);
            }
            _packagesDict.Add(package.Name, package);
        }

        var nativeList = nativePackageNamesListProvider.GetNativePackageNames().ToList();
        foreach (var packageEntry in _packagesDict)
        {
            var package = packageEntry.Value;
            
            // Mark package as native or not
            package.Native = false;
            if (nativeList.Contains(packageEntry.Key))
            {
                package.Native = true;
            }
            
            // Prepare the collection of references to dependencies
            package.Dependencies = ImmutableHashSet<IPackage>.Empty;
            foreach (var info in package.DependenciesInfos)
            {
                var reference = GetReferenceToAppropriatePackage(info);
                package.Dependencies = package.Dependencies.Add(reference);
            }
            
            // Prepare the collection of references to (installed) optional dependencies
            package.InstalledOptionalDependencies = ImmutableHashSet<IPackage>.Empty;
            foreach (var info in package.OptionalDependenciesInfos)
            {
                var reference = GetReferenceToAppropriatePackage(info, true);
                if (reference != null)
                {
                    package.InstalledOptionalDependencies = package.InstalledOptionalDependencies.Add(reference);
                }
            }
        }
    }

    /// <inheritdoc />
    public IPackage this[string packageName] =>
        !_packagesDict.ContainsKey(packageName) ? null : _packagesDict[packageName];
    
    /// <inheritdoc />
    public List<IPackage> GetInstalledPackagesConflictingWithPackage(string packageName, string packageVersion)
    {
        if (!_installedPackagesPerConflictingPackageName.ContainsKey(packageName))
            return new List<IPackage>();

        var result = new List<IPackage>();
        foreach (var package in _installedPackagesPerConflictingPackageName[packageName])
        {
            foreach (var conflictInfo in package.ConflictingPackagesInfos)
            {
                if (_versionComparer.IsVersionValid(packageVersion, conflictInfo.VersionRequirement))
                {
                    result.Add(package);
                    break;
                }
            }
        }

        return result;
    }

    /// <inheritdoc />
    public ImmutableHashSet<PackageProvider> GetInstalledPackagesProvidingPackage(string providedPackageName)
    {
        if (!_providersPerProvidedPackageDict.ContainsKey(providedPackageName))
            return ImmutableHashSet<PackageProvider>.Empty;
        return _providersPerProvidedPackageDict[providedPackageName].ToImmutableHashSet();
    }
    
    private void ProcessFileSection(IPackage package, List<string> descFileLines, int startingLineNum,
        out int endingLineNum)
    {
        var sectionName = descFileLines[startingLineNum];
        int i;
        for (i = startingLineNum + 1;; i++)
        {
            var line = descFileLines[i];
            if (descFileLines.Count <= i || string.IsNullOrEmpty(line) || line.StartsWith('%'))
            {
                i--;
                break;
            }

            switch (sectionName)
            {
                case "%NAME%":
                    package.Name = line;
                    break;
                case "%VERSION%":
                    package.Version = line;
                    break;
                case "%DEPENDS%":
                    AddDependency(package, line);
                    break;
                case "%OPTDEPENDS%":
                    AddOptionalDependency(package, line);
                    break;
                case "%PROVIDES%":
                    AddProvidedPackage(package, line);
                    break;
                case "%CONFLICTS%":
                    AddConflictingPackage(package, line);
                    break;
            }
        }

        endingLineNum = i;
    }

    private void AddDependency(IPackage package, string line)
    {
        package.DependenciesInfos = package.DependenciesInfos.Add(GetRelatedPackageInfoFromName(line));
    }
    
    private void AddOptionalDependency(IPackage package, string line)
    {
        var depName = line.Split(' ')[0];
        if (depName.EndsWith(':'))
            depName = depName[..^1];
        package.OptionalDependenciesInfos = package.OptionalDependenciesInfos.Add(GetRelatedPackageInfoFromName(depName));
    }
    
    private void AddProvidedPackage(IPackage package, string line)
    {
        var info = GetRelatedPackageInfoFromName(line);
        package.ProvidedPackagesInfos = package.ProvidedPackagesInfos.Add(info);

        var provider = new PackageProvider
        {
            ProviderName = package.Name,
            ProvidedVersion = info.VersionRequirement
        };

        if (_providersPerProvidedPackageDict.ContainsKey(info.Name))
        {
            _providersPerProvidedPackageDict[info.Name].Add(provider);
        }
        else
        {
            _providersPerProvidedPackageDict.Add(info.Name, new HashSet<PackageProvider> {provider});
        }
    }
    
    private void AddConflictingPackage(IPackage package, string line)
    {
        var conflictInfo = GetRelatedPackageInfoFromName(line);
        package.ConflictingPackagesInfos = package.ConflictingPackagesInfos.Add(conflictInfo);

        // Add to dictionary aggregating packages per conflicting package name
        if (!_installedPackagesPerConflictingPackageName.TryAdd(conflictInfo.Name, new HashSet<IPackage> { package }))
        {
            _installedPackagesPerConflictingPackageName[conflictInfo.Name].Add(package);
        }
    }

    private RelatedPackageInfo GetRelatedPackageInfoFromName(string packageFullName)
    {
        var comparisonCharacters = new List<char>
        {
            '>',
            '<',
            '='
        };
        var indexOfComparisonSubstring = comparisonCharacters
            .Select(x => packageFullName.IndexOf(x) as int?)
            .Where(x => x != -1)
            .OrderBy(x => x)
            .FirstOrDefault();

        var versionRequirement = indexOfComparisonSubstring == null ? string.Empty : packageFullName.Substring((int)indexOfComparisonSubstring);
        var name = versionRequirement == string.Empty ? packageFullName : packageFullName.Replace(versionRequirement, string.Empty);

        return new RelatedPackageInfo
        {
            Name = name,
            VersionRequirement = versionRequirement
        };
    }

    private IPackage GetReferenceToAppropriatePackage(RelatedPackageInfo info, bool ignoreMissingProviders=false)
    {
        var exceptionMessage = $"No appropriate provider found for package requirement {info.Name}";
        var notFoundException = new KeyNotFoundException(exceptionMessage);

        if (_packagesDict.ContainsKey(info.Name))
            return _packagesDict[info.Name];

        if (!_providersPerProvidedPackageDict.ContainsKey(info.Name))
        {
            if (ignoreMissingProviders)
                return null;
            throw notFoundException;
        }

        var possibleProviders = _providersPerProvidedPackageDict[info.Name];
        foreach (var provider in possibleProviders)
        {
            var providedVersionWithoutEqualSign = provider.ProvidedVersion == string.Empty ? string.Empty : provider.ProvidedVersion.Substring(1);
            if (_versionComparer.IsVersionValid(providedVersionWithoutEqualSign, info.VersionRequirement))
                return _packagesDict[provider.ProviderName];
        }

        if (ignoreMissingProviders)
            return null;
        throw notFoundException;
    }
}