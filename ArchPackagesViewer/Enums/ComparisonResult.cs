namespace ArchPackagesViewer.Enums;

/// <summary>
/// Type defining comparison results.
/// </summary>
public enum ComparisonResult
{
    LessThan = -1,
    Equal = 0,
    LargerThan = 1
}