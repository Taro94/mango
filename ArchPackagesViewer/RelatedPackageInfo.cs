namespace ArchPackagesViewer;

/// <summary>
/// Type containing information on name and version requirement of a package.
/// </summary>
public struct RelatedPackageInfo
{
    public string Name { get; set; }
    public string VersionRequirement { get; set; }
}