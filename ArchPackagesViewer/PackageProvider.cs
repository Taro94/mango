namespace ArchPackagesViewer;

/// <summary>
/// Type describing a package that provides some virtual package.
/// </summary>
public class PackageProvider
{
    public string ProviderName { get; set; }
    public string ProvidedVersion { get; set; }
}