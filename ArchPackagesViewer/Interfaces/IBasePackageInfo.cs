using System.Collections.Immutable;

namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Base package info containing its name and version.
/// </summary>
public interface IBasePackageInfo
{
    /// <summary>
    /// Gets the name of the package.
    /// </summary>
    string Name { get; set; }
    
    /// <summary>
    /// Gets the version of the package.
    /// </summary>
    string Version { get; set; }
}