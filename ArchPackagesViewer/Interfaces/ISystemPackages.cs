using System.Collections.Immutable;

namespace ArchPackagesViewer.Interfaces;

public interface ISystemPackages
{
    /// <summary>
    /// Returns the package object with the given name or null if such a package is not installed.
    /// </summary>
    /// <param name="packageName">Name of the package to retrieve</param>
    IPackage this[string packageName] { get; }

    /// <summary>
    /// Returns a list of installed packages that conflict with the package specified.
    /// </summary>
    /// <param name="packageName">Name of the package to check against.</param>
    /// <param name="packageVersion">Version of the package to check against.</param>
    /// <returns>List of packages that list the package specified as a conflict.</returns>
    List<IPackage> GetInstalledPackagesConflictingWithPackage(string packageName, string packageVersion);

    /// <summary>
    /// Returns a set of installed packages that provide the specified package name.
    /// </summary>
    /// <param name="providedPackageName">Name of the package provided.</param>
    /// <returns>Set of packages (may be empty)</returns>
    public ImmutableHashSet<PackageProvider> GetInstalledPackagesProvidingPackage(string providedPackageName);
}