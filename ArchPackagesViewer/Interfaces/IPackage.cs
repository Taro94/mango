using System.Collections.Immutable;

namespace ArchPackagesViewer.Interfaces;

/// <summary>
/// Type representing an installed Arch package.
/// </summary>
public interface IPackage : IBasePackageInfo
{
    /// <summary>
    /// Gets the status denoting whether the package is native, i.e. whether it is considered pre-installed in the OS.
    /// </summary>
    bool Native { get; set; }
    
    /// <summary>
    /// Collection of installed dependencies of the package.
    /// </summary>
    ImmutableHashSet<IPackage> Dependencies { get; set; }
    
    /// <summary>
    /// Collection of installed optional dependencies of the package.
    /// </summary>
    ImmutableHashSet<IPackage> InstalledOptionalDependencies { get; set; }

    /// <summary>
    /// Collection of objects representing basic information about packages conflicting with the package.
    /// </summary>
    ImmutableHashSet<RelatedPackageInfo> ConflictingPackagesInfos { get; set; }
    
    /// <summary>
    /// Collection of objects representing basic information about optional dependencies (installed and not installed) of the package.
    /// </summary>
    ImmutableHashSet<RelatedPackageInfo> OptionalDependenciesInfos { get; set; }
    
    /// <summary>
    /// Collection of objects representing basic information about packages provided by the package.
    /// </summary>
    ImmutableHashSet<RelatedPackageInfo> ProvidedPackagesInfos { get; set; }
    
    /// <summary>
    /// Collection of objects representing basic information about dependencies of the package.
    /// </summary>
    ImmutableHashSet<RelatedPackageInfo> DependenciesInfos { get; set; }
    
    /// <summary>
    /// Collects all dependencies (including the package itself) and returns the hash set of it.
    /// </summary>
    /// <param name="includeOptional">Will include optional dependencies if true.</param>
    /// <param name="firstLevelOptionalOnly">Will include optional dependencies of all dependencies if false; only matters if includeOptional is true.</param>
    /// <returns></returns>
    HashSet<IPackage> GetAllRequiredNonNativePackages(bool includeOptional = false, bool firstLevelOptionalOnly = true);

    /// <summary>
    /// Collects all conflicts of the package and its dependencies and returns the hash set of it.
    /// </summary>
    /// <param name="includeOptional">Will include optional dependencies if true.</param>
    /// <param name="firstLevelOptionalOnly">Will include optional dependencies of all dependencies if false; only matters if includeOptional is true.</param>
    /// <returns></returns>
    public HashSet<RelatedPackageInfo> GetAllConflicts(bool includeOptional = false, bool firstLevelOptionalOnly = true);
    
    /// <summary>
    /// Collects all packages provided by the package and its dependencies and returns the hash set of it.
    /// </summary>
    /// <param name="includeOptional">Will include optional dependencies if true.</param>
    /// <param name="firstLevelOptionalOnly">Will include optional dependencies of all dependencies if false; only matters if includeOptional is true.</param>
    /// <returns></returns>
    public HashSet<RelatedPackageInfo> GetAllProvisions(bool includeOptional = false, bool firstLevelOptionalOnly = true);
}