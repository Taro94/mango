namespace Mango;

public class MangoPackageMetadata
{
    public string MangoVersion { get; set; }
    public string PackageName { get; set; }
    public string MainPackageFilename { get; set; }
    public DateTime CreatedAt { get; set; }
    public List<string> Packages { get; set; }
    public List<string> MissingOptionalDependencies { get; set; }
    public List<string> ProvidedPackages { get; set; }
    public List<string> Conflicts { get; set; }
}