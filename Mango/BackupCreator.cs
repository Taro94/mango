using System.Reflection;
using ArchPackagesViewer.Interfaces;
using Commons;
using Mango.Interfaces;
using Mango.Options;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace Mango;

/// <inheritdoc />
public class BackupCreator : IBackupCreator
{
    private readonly ISystemPackages _systemPackages;
    private readonly IConfiguration _configuration;
    private readonly ILogger _logger;

    public BackupCreator(ISystemPackages systemPackages, IConfiguration configuration, ILogger logger = null)
    {
        _systemPackages = systemPackages;
        _configuration = configuration;
        _logger = logger;
    }
    
    /// <inheritdoc />
    public int CreateBackup(BackupOptions options)
    {
        _logger?.Information($"Backup creation starting for options: package = {options.Package}, directory = {options.Directory}, rawPackages = {options.RawPackages}, withOptionalDependencies = {options.WithOptionalDependencies}");
        
        var temporaryDirectoryPath = _configuration.GetRequiredSection("TemporaryDirectoryPath")?.Value;
        if (temporaryDirectoryPath == null)
        {
            var fatal = "Temporary directory path is null, appsettings.json was not configured properly at build time.";
            _logger?.Fatal(fatal);
            Console.Error.WriteLine("Fatal error, see the log file for details.");
            return 1;
        }

        // Delete temporary directory
        if (Directory.Exists(temporaryDirectoryPath))
            Directory.Delete(temporaryDirectoryPath, true);
        
        // Ensure directory exists
        if (options.Directory != null && !Directory.Exists(options.Directory))
        {
            var error = $"Directory {options.Directory} does not exist.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        
        // Ensure the output file doesn't exist (if not in raw mode)
        if (!options.RawPackages)
        {
            var filePath = options.Package + ".mango";
            if (options.Directory != null)
                filePath = Path.Combine(options.Directory, filePath);
            
            if (File.Exists(filePath))
            {
                var error = "File already exists at this location.";
                _logger?.Error(error);
                Console.Error.WriteLine(error);
                return 1;
            }
        }
        
        // Get package and do basic checks
        var package = _systemPackages[options.Package];
        if (package == null)
        {
            var error = $"Package '{options.Package}' is not installed.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }
        if (package.Native)
        {
            var error = $"Package '{options.Package}' is a native package.";
            _logger?.Error(error);
            Console.Error.WriteLine(error);
            return 1;
        }

        // Get packages
        var dependencies = package.GetAllRequiredNonNativePackages(options.WithOptionalDependencies);
        var packageNamesWithVersions = dependencies.Select(x => $"{x.Name} {x.Version}").ToList();
        dependencies.RemoveWhere(x => x.Name == options.Package);
        var dependencyNames = dependencies.Select(x => x.Name).ToList();

        // Get names of missing optional dependencies
        List<string> missingOptional;
        if (!options.WithOptionalDependencies)
        {
            missingOptional = package.OptionalDependenciesInfos.Select(x => x.Name + x.VersionRequirement).ToList();
        }
        else
        {
            missingOptional = package.OptionalDependenciesInfos
                .Where(x => package.InstalledOptionalDependencies.All(y => x.Name != y.Name))
                .Select(x => x.Name + x.VersionRequirement)
                .ToList();
        }

        var packagesCount = dependencyNames.Count + 1;
        var currentPackage = 1;

        // Deal with the primary package
        var info = $"Reassembling package: {options.Package} ({currentPackage}/{packagesCount})";
        _logger?.Information(info);
        Console.WriteLine(info);
        var mainFilename = CreatePackageBackup(temporaryDirectoryPath, options.Package);
        currentPackage++;
        
        // Deal with other packages
        foreach (var dependency in dependencyNames)
        {
            info = $"Reassembling package: {dependency} ({currentPackage}/{packagesCount})";
            _logger?.Information(info);
            Console.WriteLine(info);
            CreatePackageBackup(temporaryDirectoryPath, dependency);
            currentPackage++;
        }
        
        var outputDir = options.Directory ?? Directory.GetCurrentDirectory();
        
        // Raw backup
        if (options.RawPackages)
        {
            foreach (var file in Directory.GetFiles(temporaryDirectoryPath))
                File.Move(file, Path.Combine(outputDir, Path.GetFileName(file)));

            Directory.Delete(temporaryDirectoryPath, true);
            info = "Raw backup(s) created successfully!";
            _logger?.Information(info);
            Console.WriteLine(info);
            return 0;
        }
        
        // Get conflicts and provisions
        var conflicts = package.GetAllConflicts(options.WithOptionalDependencies)
            .Select(x => $"{x.Name}{x.VersionRequirement}").ToList();
        var provisions = package.GetAllProvisions(options.WithOptionalDependencies)
            .Select(x => $"{x.Name}{x.VersionRequirement}").ToList();
        
        // Mango metadata file
        var metaData = JsonConvert.SerializeObject(new MangoPackageMetadata()
        {
            PackageName = options.Package,
            MainPackageFilename = mainFilename,
            CreatedAt = DateTime.Now,
            Packages = packageNamesWithVersions,
            MissingOptionalDependencies = missingOptional,
            MangoVersion = string.Concat(Assembly.GetExecutingAssembly().GetName().Version!.ToString().Reverse()
                .Skip(2).Reverse()),
            Conflicts = conflicts.ToList(),
            ProvidedPackages = provisions.ToList()
        });
        File.WriteAllText(Path.Combine(temporaryDirectoryPath, ".MANGO"), metaData);
        
        // Mango package assembly
        var mangoFilename = $"{options.Package}.mango";
        $"cd {temporaryDirectoryPath} ; tar -cf {mangoFilename} * .MANGO".Bash();
        File.Move(Path.Combine(temporaryDirectoryPath, mangoFilename), Path.Combine(outputDir, mangoFilename));

        Directory.Delete(temporaryDirectoryPath, true);

        info = "mango package created successfully!";
        _logger?.Information(info);
        Console.WriteLine(info);
        
        return 0;
    }
    
    private string CreatePackageBackup(string tmpDirPath, string package)
    {
        Directory.CreateDirectory(tmpDirPath);
        var originalFiles = Directory.GetFiles(tmpDirPath);

        var command = $"/usr/share/mango/fakepkg.sh -o {tmpDirPath} {package}";
        command.Bash();
        var newFiles = Directory.GetFiles(tmpDirPath);
        var filename = newFiles.Except(originalFiles).FirstOrDefault();
        return Path.GetFileName(filename);
    }
}