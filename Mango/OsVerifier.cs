using Commons;
using Mango.Interfaces;

namespace Mango;

/// <inheritdoc />
public class OsVerifier : IOsVerifier
{
    /// <inheritdoc />
    public bool GetIsOsArchLinux()
    {
        return File.Exists("/etc/arch-release");
    }
}