using System.Reflection;
using Commons;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Mango;

/// <summary>
/// Configuration of services for the host builder.
/// </summary>
public static class ConfigureServicesExtensions
{
    private static IConfiguration _configuration;

    public static IServiceCollection AddConfiguration(this IServiceCollection services)
    {
        return services.AddSingleton(x => GetConfiguration());
    }
    
    public static IServiceCollection AddLogging(this IServiceCollection services)
    {
        

        var loggerConfig = new LoggerConfiguration()
            .ReadFrom.Configuration(GetConfiguration());
            
        return services.AddSingleton<ILogger>(_ => loggerConfig.CreateLogger());
    }

    private static IConfiguration GetConfiguration()
    {
        if (_configuration != null)
            return _configuration;
            
        var homePath = "echo $HOME".Bash().Trim('\n');
        var appSettingsStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Mango.appsettings.json");
        if (appSettingsStream == null)
            throw new NullReferenceException(
                "Null Mango.appsettings.json stream: ensure the file's build action is EmbeddedResource.");
            
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonStream(appSettingsStream, new List<(string, string)>
            {
                ("$HOME", homePath)
            })
            .Build();
            
        _configuration = configuration;
        return _configuration;
    }
}