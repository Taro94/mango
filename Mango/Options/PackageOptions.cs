using CommandLine;

namespace Mango.Options;

[Verb("package", HelpText = "Display information about a Mango package file.")]
public class PackageOptions {
        
    [Value(0, HelpText = "Mango package file to print metadata of.", Required = true)]
    public string Package { get; set; }
}