using CommandLine;

namespace Mango.Options;

[Verb("install", HelpText = "Install a package with its dependencies from a mango package file.")]
public class InstallOptions {
        
    [Value(0, HelpText = "Mango package file to install.", Required = true)]
    public string Package { get; set; }
    
    [Option('d', "downgrade", Required = false, HelpText = "Downgrade installed Arch packages to versions from the mango package (not recommended).")]
    public bool Downgrade { get; set; }
}