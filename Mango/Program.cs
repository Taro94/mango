﻿using System.Reflection;
using ArchPackagesViewer;
using ArchPackagesViewer.Interfaces;
using CommandLine;
using Mango.Interfaces;
using Mango.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Mango;

internal static class Program
{
    private static int Main(string[] args)
    {
        var host = CreateHostBuilder(args).Build();
        var logger = host.Services.GetRequiredService<ILogger>();
        logger.Information("mango starting");
        
        var distroVerifier = host.Services.GetRequiredService<IOsVerifier>();
        if (!distroVerifier.GetIsOsArchLinux())
        {
            logger.Error("OS not ArchLinux-based, quitting");
            Console.Error.WriteLine("Oops! Your operating system appears to not be based on Arch Linux. Mango is a tool useful only for users of Arch Linux distributions (including Manjaro Linux). Come again when you have Arch!");
            return 1;
        }
        
        return Parser.Default.ParseArguments<BackupOptions, InstallOptions, PackageOptions>(args)
            .MapResult(
                (BackupOptions opts) => host.Services.GetRequiredService<IBackupCreator>().CreateBackup(opts),
                (InstallOptions opts) => host.Services.GetRequiredService<IPackageInstaller>().InstallPackage(opts),
                (PackageOptions opts) => host.Services.GetRequiredService<IPackageInfoPrinter>().PrintPackageInformation(opts),
                errs => 1 );
    }

    private static IHostBuilder CreateHostBuilder(string[] args)
    {
        // There seems to be a dotnet issue that makes CreateDefaultBuilder call hang if the working directory is not the same as the executing program's location
        var workingDir = Directory.GetCurrentDirectory();
        Directory.SetCurrentDirectory(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule!.FileName)!);
        
        var host = Host.CreateDefaultBuilder(args)
            .ConfigureServices((hostContext, services) =>
            {
                services
                    .AddSingleton<IOsVerifier, OsVerifier>()
                    .AddSingleton<IRootAccessVerifier, RootAccessVerifier>()
                    .AddSingleton<IBackupCreator, BackupCreator>()
                    .AddSingleton<IPackageInfoPrinter, PackageInfoPrinter>()
                    .AddSingleton<IPackageInstaller, PackageInstaller>()
                    .AddSingleton<IPackageFactory, PackageFactory>()
                    .AddSingleton<IVersionComparer, VersionComparer>()
                    .AddSingleton<INativePackagesListProvider>(provider =>
                    {
                        var configuration = provider.GetRequiredService<IConfiguration>();
                        var nativePkgsPath = configuration.GetRequiredSection("NativePkgsListPath").Value;
                        return new NativePackagesListProvider(nativePkgsPath);
                    })
                    .AddSingleton<ISystemPackages, SystemPackages>()
                    .AddSingleton<IConflictDetector, ConflictDetector>()
                    .AddConfiguration()
                    .AddLogging();
            });
        
        Directory.SetCurrentDirectory(workingDir);
        
        return host;
    }
}
