using Commons;
using Mango.Interfaces;

namespace Mango;

/// <inheritdoc />
public class RootAccessVerifier : IRootAccessVerifier
{
    /// <inheritdoc />
    public bool GetHasRootAccess()
    {
        var result = $"if [[ $EUID -ne 0 ]]; then echo \"{false}\"; else echo \"{true}\"; fi".Bash();
        return bool.Parse(result);
    }
}