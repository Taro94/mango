using Mango.Options;
using Serilog;

namespace Mango.Interfaces;

/// <summary>
/// Service to create package backups.
/// </summary>
public interface IBackupCreator
{
    /// <summary>
    /// Creates a backup of an Arch package.
    /// </summary>
    /// <param name="options">Object containing configuration of the command.</param>
    /// <returns>0 if successful, 1 in case of errors.</returns>
    int CreateBackup(BackupOptions options);
}