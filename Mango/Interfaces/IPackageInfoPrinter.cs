using Mango.Options;

namespace Mango.Interfaces;

/// <summary>
/// Service to display information on mango packages.
/// </summary>
public interface IPackageInfoPrinter
{
    /// <summary>
    /// Prints information on a mango package.
    /// </summary>
    /// <param name="options">Object containing configuration of the command.</param>
    /// <returns>0 if successful, 1 in case of errors.</returns>
    int PrintPackageInformation(PackageOptions options);
}