# Mango

This is a tool that allows you to back up pacman packages (for ArchLinux-based operating systems, such as Manjaro or ArchLinux itself) along with all their dependencies other than those pre-installed on your OS.

## But why?

There seemed to be no easy way of creating a full backup of a pacman package. 

You could download a package with pacman without installing it, but then you'd need to manually do the same for all its dependencies and their dependencies. This also won't let you back up packages after they've been installed without redownloading them. 

The latter can be remedied with scripts such as [fakepkg](https://github.com/Edenhofer/fakepkg) that allow you to reassembly installed packages from your filesystem, which is great, but still doesn't help you with the package's dependencies in any way.

## Ok, but why create backups at all?

For a variety of reasons:
- You may want to create a backup to act as an offline installer if you know you'll need to install a specific package on an offline system.
- You may want to create a backup of a specific version of some software in case some future version introduces problems causing you to want to roll back to a previous one.
- It's not unheard of for packages to break due to one of their dependencies breaking or disappearing.

## Fair enough, where to get it?

You can get it on [AUR](https://aur.archlinux.org/packages/mango). This tool is only useful on Arch-based systems, so there's no need to distribute it via any other means.

However, if you'd like to use mango to install some backed up packages on an offline system, you'll want to have some means of installing mango offline, as well. If that's your situation, you can simply use mango to back up itself. Mango normally produces a *.mango package which can be only installed by mango, but you can use the -r or --raw flag to create loose *.pkg.tar.xz packages. Mango has no dependencies (other than tar and pacman, which should be pre-installed in every Arch-based system anyway), so the result of the `mango backup -r mango` command will be a single pacman package you can copy over to your offline system and install there via pacman.

## Native (pre-installed) packages

Some packages may be pre-installed on your operating system. You'll typically not want to include them in your mango packages even if they are dependencies required by the package you are creating a backup of. Manjaro keeps the list of pre-installed packages in /desktopfs-pkgs.txt and mango on Manjaro will use that file to determine which packages do not need to be included. 

However, on other systems it's impossible for mango to know which packages are pre-installed by itself and the list may vary greatly depending on your OS, its edition and version.

As such, you need to tell mango which packages you consider to be native (pre-installed). You can do that by preparing a simple text file under path `~/.local/share/mango/native`, listing all the native package names (without versions), one per line. You can easily create such a file containing currently installed packages using the following command: `mkdir ~/.local/share/mango -p && pacman -Qq > ~/.local/share/mango/native`.

You can use a custom native packages list even on Manjaro. If you use one, mango will use it instead of the list provided by the OS.

If you absolutely want mango to include any and all required dependencies in its backups (to make them installable on any Arch-based OS), you can create an empty file and use it as a native package list. This is not recommended due to the possible large size of created backups and the possible inclusion of some low-level stuff.

When using the native packages list, make sure you don't run `mango backup <package>` as root, because the tool will not check your usual user directory path when searching for the `native` file.
